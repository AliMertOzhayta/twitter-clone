//___FILEHEADER___

import SwiftUI

@main
struct TwitterClone: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
