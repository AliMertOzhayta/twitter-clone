//
//  ContentView.swift
//  Twitter - Clone
//
//  Created by Ali Mert Özhayta on 23.05.2021.
//

import SwiftUI

struct RootView: View {
    var body: some View {
        NavigationView {
            TabView {
                Text("Home")
                    .tabItem {
                        Label("Home", systemImage: "house")
                    }
                
                Text("Search")
                    .tabItem {
                        Label("Search", systemImage: "magnifyingglass")
                    }
                
                Text("Messages")
                    .tabItem {
                        Label("Messages", systemImage: "envelope")
                    }
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
